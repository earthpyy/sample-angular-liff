import {Component, OnInit} from '@angular/core';
import {environment} from '../environments/environment';
import * as LINE from '@line/bot-sdk';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  loading = true;
  isLoggedIn: boolean;

  profile: LINE.Profile;

  ngOnInit(): void {
    this.initLiff();
  }

  initLiff(): void {
    const liffId = environment.liffId;

    liff.init({ liffId })
      .then(() => {
        this.isLoggedIn = liff.isLoggedIn();

        if (this.isLoggedIn) {
          this.loadProfile();
        } else {
          this.loading = false;
        }
      })
      .catch(() => this.loading = false);
  }

  loadProfile(): void {
    liff.getProfile().then((profile: LINE.Profile) => {
      this.profile = profile;
    });
  }
}
