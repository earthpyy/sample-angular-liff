# Sample Angular LIFF

## Description
Sample [Angular](https://angular.io/) + [LINE LIFF](https://developers.line.biz/en/docs/liff/developing-liff-apps/) Integration.  
The application simply say hello to user's LINE profile name :)

## Screenshot
![hello](screenshots/hello.png "Hello")

## Set-up Project
### Requirements
- node 12.8.1
- npm 6.14.5

### Steps
1. `cd` into project root
```
$ cd angular
```

2. Install dependencies
```
$ npm install
```

3. Start application
```
$ npm run serve
```

## Deploy to Heroku
### Requirements
- [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)

### Steps
1. Install Heroku (if not installed)
```
$ brew tap heroku/brew
$ brew install heroku
``` 

2. Login with Heroku
```
$ heroku login
```

3. Add Heroku Git remote
```
$ heroku git:remote -a sample-angular-liff
```

4. Make sure that the build packs include `nodejs`
```
$ heroku buildpacks:set heroku/nodejs
```

5. If there is any change made, push them to Heroku
```
$ git subtree push --prefix angular heroku master
```
